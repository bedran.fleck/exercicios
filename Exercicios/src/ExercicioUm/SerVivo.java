/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ExercicioUm;

/**
 *
 * @author andre
 */
public class SerVivo {
    private boolean vida;
    

    public SerVivo(boolean vida) {
        this.vida = vida;
    }

    public boolean isVida() {
        return vida;
    }

    public void setVida(boolean vida) {
        this.vida = vida;
    }
    
    public void comer(){
        vida = true;
    }
        
    public void reproduzir(){
        vida = true;
    }

    public void excretar(){
        vida = true;
    }
    
    public void descansar(){
        vida = true;
    }
    
    public boolean morrer(){
        vida = false;
        setVida(vida);
        return false;
    }
}
