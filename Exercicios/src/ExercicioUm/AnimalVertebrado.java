/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ExercicioUm;

import java.util.ArrayList;

/**
 *
 * @author andre
 */
public class AnimalVertebrado extends Animal{

    private final ArrayList<String> listaOssos;
    private final ArrayList<String> listaMusculos;
    private final boolean bigScale;

    public AnimalVertebrado(ArrayList<String> listaOssos, ArrayList<String> listaMusculos, boolean bigScale, boolean hasBrain, boolean vida) {
        super(hasBrain, vida);
        this.listaOssos = listaOssos;
        this.listaMusculos = listaMusculos;
        this.bigScale = bigScale;
    }
    
    
    
    public void mexerEsqueleto(){
    }
    
    
}
