/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ExercicioUm;


/**
 *
 * @author andre
 */
public class Animal extends SerVivo{

    private boolean hasBrain;

    public Animal(boolean hasBrain, boolean vida) {
        super(vida);
        this.hasBrain = hasBrain;
    }

    public void setHasBrain(boolean hasBrain) {
        this.hasBrain = hasBrain;
    }

    public boolean isHasBrain() {
        return hasBrain;
    }

    public Animal(boolean vida) {
        super(vida);
    }    
    
    
}
