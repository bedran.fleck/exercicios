/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ExercicioUm;

/**
 *
 * @author andre
 */
public class Protista extends SerVivo{
    
    private boolean tinyScale;
    private boolean dangerous;
    private boolean lethal;
    private boolean healthy;

    public Protista(boolean tinyScale, boolean dangerous, boolean lethal, boolean healthy, boolean vida) {
        super(vida);
        this.tinyScale = tinyScale;
        this.dangerous = dangerous;
        this.lethal = lethal;
        this.healthy = healthy;
    }

    public boolean isTinyScale() {
        return tinyScale;
    }

    public boolean isDangerous() {
        return dangerous;
    }

    public boolean isLethal() {
        return lethal;
    }

    public boolean isHealthy() {
        return healthy;
    }

    public void setTinyScale(boolean tinyScale) {
        this.tinyScale = tinyScale;
    }

    public void setDangerous(boolean dangerous) {
        this.dangerous = dangerous;
    }

    public void setLethal(boolean lethal) {
        this.lethal = lethal;
    }

    public void setHealthy(boolean healthy) {
        this.healthy = healthy;
    }
    
    public void infectOrganism(){
    }
    
    public void resistTreatment(){
    }
    
    public void spread(){
    }
    
}
