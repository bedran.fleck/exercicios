/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ExercicioUm;

/**
 *
 * @author andre
 */
public class Fungo extends SerVivo{
    
    private boolean tinyScale;
    
    private boolean community;
    
    private boolean exponentialGrow;

    public Fungo(boolean tinyScale, boolean community, boolean exponentialGrow, boolean vida) {
        super(vida);
        this.tinyScale = tinyScale;
        this.community = community;
        this.exponentialGrow = exponentialGrow;
    }

    public boolean isTinyScale() {
        return tinyScale;
    }

    public boolean isCommunity() {
        return community;
    }

    public boolean isExponentialGrow() {
        return exponentialGrow;
    }

    public void setTinyScale(boolean tinyScale) {
        this.tinyScale = tinyScale;
    }

    public void setCommunity(boolean community) {
        this.community = community;
    }

    public void setExponentialGrow(boolean exponentialGrow) {
        this.exponentialGrow = exponentialGrow;
    }
    
    public void fermentar(){
    }
    
}
