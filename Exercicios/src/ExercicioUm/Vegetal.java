/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ExercicioUm;

/**
 *
 * @author andre
 */
public class Vegetal extends SerVivo{
    
    private boolean bigScale;
    private boolean completelyStatic;
    private boolean lightDependant;

    public Vegetal(boolean bigScale, boolean completelyStatic, boolean lightDependant, boolean vida) {
        super(vida);
        this.bigScale = bigScale;
        this.completelyStatic = completelyStatic;
        this.lightDependant = lightDependant;
    }

    public boolean isBigScale() {
        return bigScale;
    }

    public boolean isCompletelyStatic() {
        return completelyStatic;
    }

    public boolean isLightDependant() {
        return lightDependant;
    }

    public void setBigScale(boolean bigScale) {
        this.bigScale = bigScale;
    }

    public void setCompletelyStatic(boolean completelyStatic) {
        this.completelyStatic = completelyStatic;
    }

    public void setLightDependant(boolean lightDependant) {
        this.lightDependant = lightDependant;
    }
    
    public void photosynthesys(){
    }
    
    public void greenWorld(){
    }
    
}