/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ExercicioUm;

/**
 *
 * @author andre
 */
public class AnimalInvertebrado extends Animal{
    
    private boolean exoesqueleto;
    private boolean smallScale;

    public boolean isExoesqueleto() {
        return exoesqueleto;
    }

    public boolean isSmallScale() {
        return smallScale;
    }

    public void setExoesqueleto(boolean exoesqueleto) {
        this.exoesqueleto = exoesqueleto;
    }

    public void setSmallScale(boolean smallScale) {
        this.smallScale = smallScale;
    }

    public AnimalInvertebrado(boolean exoesqueleto, boolean smallScale, boolean hasBrain, boolean vida) {
        super(hasBrain, vida);
        this.exoesqueleto = exoesqueleto;
        this.smallScale = smallScale;
    }
    
    public void mexerExoesqueleto(){
    }
    
    
    
}
