/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ExercicioDois;

import static java.lang.Math.sqrt;

/**
 *
 * @author andre
 */
public class TrianguloRetangulo extends Triangulo{

    public TrianguloRetangulo(double lado1, double lado2, double lado3) {
        super(lado1, lado2, lado3);
    }
    
    
    public double retangulo(){
        double cateto1 = super.getLado2();
        double cateto2 = super.getLado3();
        double hipotenusa = sqrt((cateto1*cateto1)+(cateto2*cateto2));
        super.setLado1(hipotenusa);
        return hipotenusa;
    }
}
