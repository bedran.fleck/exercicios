/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ExercicioDois;

/**
 *
 * @author andre
 */
public class Losango extends Retangulo{
    
    private double diagonal1;
    private double diagonal2;

    public Losango(double diagonal1, double diagonal2, double lado1, double lado2, double lado3, double lado4) {
        super(lado1, lado2, lado3, lado4);
        this.diagonal1 = diagonal1;
        this.diagonal2 = diagonal2;
    }
    
    public double getDiagonal1() {
        return diagonal1;
    }

    public double getDiagonal2() {
        return diagonal2;
    }

    public void setDiagonal1(double diagonal1) {
        this.diagonal1 = diagonal1;
    }

    public void setDiagonal2(double diagonal2) {
        this.diagonal2 = diagonal2;
    }
    
    public void losango(){
        diagonal1 = diagonal2;
    }
    
}
